<?php

namespace Drupal\sri;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class SriServiceProvider.
 *
 * @package Drupal\sri
 */
class SriServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('asset.js.collection_renderer');
    $definition->setClass('Drupal\sri\JsCollectionRenderer');
    $definition = $container->getDefinition('asset.css.collection_renderer');
    $definition->setClass('Drupal\sri\CssCollectionRenderer');
  }

}
